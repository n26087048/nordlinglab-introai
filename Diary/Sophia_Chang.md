This diary file is written by Sophia Chang I54061314 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-02-20 #

* I know nothing about AI,so glad to have a start.
* Hope that I have enough courage to raise my hand and outspeak my thoughts in good timing.
* This course gives a chance for me to touch new Internet source.

# 2019-02-27 #

* For me, AI is ‘’computers that can learn themselves when giving them a start. ’’
* My partner’s answer is ‘’Imitation of human intelligence.’’ I felt confuse about it.
* ‘’Intelligence’’ is the word what I confuse, do we really know what human intelligence are? Why people use a confusing noun to describe another confusing word?
* AI is a word relative new, if we take a research of the history about the field and find the first use of this word, would it help us to define?
* These words are groups. People tend to put things in an order.
* So many things look a bit same but still different; they might have some connection but we still don’t know how to combine them. Thus we give a name first, like a process working forward to the Grand Unification.
* I like teacher’s presence of these words.
* These words are not totally isolated , using the graph can explain their relation more clear than using words.
* Like what teacher tell us, what one prefer may not be other people prefer. Those meanings all exist at the same time.
* So far I’ve learn many words in a total different field.

# 2019-03-06 #
* Python is a brand new language for me!
* I think there might be ways to get into a same exacution.
* How do programming languages communicate with each other?
* I am so looking forward for seeing the applications of Python. Or maybe I've already seen or even use the outcomes,not knowing that they are from Python.
* How do the computer knows ''math.sin( )'' is for calculating not just for the word ''sin''?
* Is it what AI for? Enable to tell all the verb we enter for exacution.

# 2019-03-13 #
* I met a great partner from Malaysia!
* My partner taught me many basic knowledge about Python and show me right a way.
* Python is so interesting!But I need time to get used to it.
* Printing strings with different signs and getting outcomes as what wrote on PPT last week is so cool!
* Thanks for the course giving such a good chance! My partner said that the website we worked today must be written in lots of effort.
* This is the first class that truly gives me a chance to learn by myself instead of being pushed by the pressure of test.
* Hope that I can soon open a file by Python!

# 2019-03-20 #
* First time I see the word 'neural network',I thought of neural system in human body. And it goes from a stimulation,which induce the change of iron composition and lead to a current as well as pathway for reaction.
* Then I feel neural network just like the math f(x).We throw numbers inside and it gives us an answer.
* It goes layer and layer become more and more precisely. I felt more specific to call the 'learning process' as 'choosing process'.
* As for different information needed at the cake page,sorry I forgot the key words,it is more like the process 'human see things'.First,different lights come into our eyes as many basement information needed;Second,those lights form images on retina at a reverse form,and our brain revise it at the same time add many other information like moods or feelings inside as the process we can't see but do the most;Last, we see what we see as the outcome.
* I need to catch up for the schedule.

# 2019-03-27 #
* I first know typeI error and typeII error on my biostatistics course. I feel confuse about why we go from the wrong answers at the time.
* In neural network,computer learn from excluding wrong answers,so as human.We don't know what is correct,but we can experience mistakes.
* "All models are wrong,but some are useful."
* Our answer is that models now are not in their final step,they still need to be complete.
* I think models are made of hypothesis,it might not be a confirm base.
* Models vs real system is really interesting!

# 2019-04-10 #
* Beside the gradient,loss data and misfunction might cause the valley of the landscape.
* We learn filter today,it made me feel like a converse way of training.
* I finally know why my high school math teacher said matrix is about computer.
* What determine our way to the lowest spot? Distance,angle,or times we tried?
<<<<<<< HEAD
<<<<<<< HEAD

# 2019-04-17 #
* Learning TensorFlow today. Information exploded in my brain. I must studied harder about the courses before.
* We can write our own website based on python! So cool.
* Why can it only read the number? Before the number, isn't it compose by symbols?
* Feel like how things exist from nothing.

# 2019-04-24 #
* We practice TensorFlow today. Seeing commands working was so excited.
* Professor taught me add a testing line "print(test_data)". And I can see if it works clearly!
* I made a "value error" even just following the steps,so funny.

# 2019-05-01 #
* Still practicing TensorFlow today.
* I got too many problems to list what they are,so sad.
* I think I can't type these without instruction.
* Maybe I'll find my problems within this week,and I'll update them in the diary then.

# 2019-05-08 #
* I always ask my brother how computer or smartphone work,but I didn't realize that I'm learning myself now, haha.
* Even the entry of this field is so difficult,however,I think it's my problem that I didn't catch up with the course. Thanks for the review today,I might know what direction to study.

# 2019-05-15 #
* We learned what life would look like in the future with the interfere of industrial revolution.
* I want to be a surgeon. Though people say AI might replace some kinds of doctors,I don't think so. I think AI develop based on what people already know,like big data,however,we don't really know exactly how human body works,let alone AI.
* I think occupation change will oscillate in the future. After years of convenience,people might miss the communication before. Who knows,misterious future.
* We use renewable energy for the environment,but we may cost more or hurt more environment for building the facilities.
* Energy storage and transportion are so important! 

# 2019-05-22 #
* I learn many ethics topics since I came to the university. So many situation examples and stories about ethics and moral,however,I think only the people involved can figure out the answer,or maybe there is no answer.
* I liked the first TED talk! She say we make the choice! It is we human being to chose the direction and velocity of AI development!
* Ethics topics are important. Indeed,if one day things run out of control,it might because of someone lost his ethics.
* Today we saw that AI can produce video about a person talking without "the person" acting! So horrible.
* About dark skin color,I think it's just because people haven't enter data. It's not fair,but maybe the environment before don't need this data. For example, I seldom see dark skin people in where I live. I'd like to say that it is more about supply and demand balance but discrimination.

# 2019-05-29 #
* First day  for group work!
* I learn a lot from working in group and discussion. Group members looked so friendly and powerful.

# 2019-06-06 #
* We trained our models with many different way today,and also bring what we did at hom last week.
* Watching the accuracy runs higher really makes me happy,however,some little change will make results become ugly.
* Process of training feels like doing a research.Try from many bad results,though we don't know when we will success or even will we success is a big question.
* My friend taught me to import convolution 2D and lit up the epochs.It works!

# 2019-06-12 #
* Seeing other teams report to day , I learn a lot and found that searching useful information can get a good result more efficiently.
* Other teams record their results in different ways. I should record my results,too. These records can tell lots of things and let me not making same mistakes.

# 2019-06-19 #
* Incredible,a semester just end. I never thought that I could made it joining a class with totally different field and with unfamiliar language.
* I felt really nervous about final exam.Especially when I realized 3 min left but I still have many questions without answers.
* This is the most interesting class I took in NCKU. I love the moment when our accuracy break 0.99. Thank you for giving me a chance to really involve in what I never touch.