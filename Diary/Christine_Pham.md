
This is the studying diary of Christine PHam. 

# 19/02/27 Class 2 Statistics, Data Science, Data Mining, System Identification, AI, Machine Learning, Deep Learning #

So the 1 hour of lecture is not enough for me,to be honest after Professor introduced them, I am still confused about how they can be distinguished.

So these are all the fields related to data processing. Each of the fields has its own definitions. 
But if only looking at those definitions, it's only just English letter for me. 

So I read more 
And I found out that they appeared in different historical milestones. 
--> I think this is a good way to understand it as a story.

Statistics: started from the 17th century, strong relationship with probability theory. 
One of the very famous questions yo in Statistics is that �What is the chance that event can happen?�. 
Later it was extended to include the analysis and interpretation of data. 
Statistics is the key application to a wide range of sciences.

Data Science: Practical statistics + computer, 
started in the 20th century when the birth of computer changed the way we understand data. 
But until 2001, this term was introduced by Cleveland as an independent discipline. 
A Data Scientist has the skills of a general statistician and a good understanding of how software work,
as well as multiple programming languages.

Data Mining: While data science focuses on the science of data, data mining is concerned with the process.
It mines the data and predicts the patterns. It deals with the process of discovering newer patterns in big data sets.
I believe people have tried to mine the data from the day it was born, but it was not convenient back to those time. 
The process of mining the data has evolved over time alongside with the development of computer and internet advancement. 
For me, I think Data Mining is a subset of Data Science.

System Identification:  the task of constructing a dynamical model that can predict the outputs of a dynamical system. 
The root was from the 19th century. 
The idea of System Identification is to capture the movement of the database that runs over time 
(Stock Price in the financial market) 
I'm not familiar with all the engineerning things so this way it makes senses to me.

Artificial Intelligence: 20th century when the idea of building an electrical brain was introduced. 
boomed in the 21 century with the presence of powerful computer hardware. this is a subset of Data Science.

Machine Learning: an application of artificial intelligence that provides systems the ability to automatically learn 
and improve from experience without being explicitly programmed. 
If we say AI is an electrical brain, then Machine learning is one of the best ability that the brain can perform.

Deep Learning: One way of machine learning, but more powerful. 
I would like to borrow this picture to explain my understanding of it. 
(So the picture by YannLeCun is not uploadable here so I will explain by words)
You want to see a fancy car, 
normal learning will just see it and show you: oh this is car.
normal leaning level 2 will have one more look and tell you: oh this is a yellow car. 
deep learning: this is a tesla car, painted yellow, function A of the car, function B of the car. 

Deep Learning = Learning Hierarchical Representations

All of the above thoughts are just my personal discovery. It might not correct, but I hope it is. 