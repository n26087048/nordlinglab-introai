This diary file is written by Jose Chavez E24057081 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-21

* I learned about AI in board games

* The same technology may be used in the future for diagnostics

* I'm curious about how group work will be handled in this online class

# 2020-03-29

* I hope we can get into Python soon

# 2020-04-05

* Watched the video on neural networks

# 2020-04-12

* I took a look at the python tutorial

# 2020-04-19

* I read the new powerpoint slides provided

* I continued to practice python

# 2020-04-26

* I did the reading on Machine learning by Ian Goodfellow and Yoshua Bengio and Aaron Courville

* The way "learning" is characterized in terms of P performance, E experience, and T tasks is very intuitive

* I learned there are several types of tasks such as: classification, regression, and transcription

# 2020-05-06

* I watched the video series on introduction to neural networks on the three blue one brown youtube channel

* I finnished the tensorflow exaples on the colab page, doing the excercice left at the end I achieved an improvement on accuracy of arround 1%, from 96 to 97

* I'm researching ways to improve the nn model to further increase accuracy and robustness of the model, bt for example performing random rotations of adding blur to the input images during trainig

* Tensorflow is a very compleate package and gratly simplyfies those tasks