
This diary file is written by Parichat Lertpanomthong N26087022 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-21 #

* From The performance graph that compare between human and deep learning model,so in the future we can have more efficientcy deep learning application that suit for each works more than human doing.
* Deep learing has Supervised learning,Supervised learning, Reinforcement learing. 
* The presentation shown many applications.
* Althrough AI is good technology in many areas, but some AI cannot achieve some tasks for example the Waymo accident rate of autonomous car is higher than the accident rate of most experienced drivers.
* Maybe in the future both of prediction and mechanistic can work well with AI without human interuption refer from the performance graph.

# Article summary #
* The journal name is "An Artificial Intelligence-Based System to Assess Nutrient Intake for Hospitalised Patients"
* Ya Lu, Thomai Stathopoulou, Maria F. Vasiloglou, Stergios Christodoulidis, Zeno Stanga, Stavroula Mougiakakou,  August 2015
* The propose is to accuracy estimate nutrient intake by AI using simply processing RGB Depth (RGB-D) image pairs captured before and after meal consumption.
* This is supervised learning by limited training samples for food recognition.
* Relative errors less than 20%

# 2020-03-29 #

* Learn about "What is AI" article
* The example application of AI such as self-driving cars, content recommendation, image and video processing
* Implication of each application for example the safty of self-driving car need to be more than human level, For content reccomendation need to be aware of fake things as news and for Image and video processing has more advance to distinguish which is real video.
* Dont have exact definition of AI yet. AI is not the new area but it is gonna be more taught.
* AI is complicated to understand but it is actually easy.
* The related field is Deep learning, Data science, Robotics
* Deep learning is a subfield of machine learning. This refer to the complexity of a mathematical model and lead us to do complexity level.
* Robotics means building and programming robots in real world scenarios.

# Article summary #

* Reading article from https://www.infoworld.com/article/3512245/deep-learning-vs-machine-learning-understand-the-differences.html
* The article is "Deep learning vs. machine learning: Understand the differences"
* Machine learing and Deep learing are subset of AI. Deep learing is specific kind of Machine learning. 
* Both need training and test dataset
* Deep learning model can produce better fits model than Machine learning model.

# 2020-04-12 #

* Due to last week was the national holiday and spring break. So we don't have class and homework in this course.
* I follow "Homework assignment" topic. So i studied and practiced python by running through Colaboratory.
* I studied basic programming in python3 including comments, printing, loops, variables and functions etc.
* I also installed python3.6 in Anaconda environment for learning more about deep learning in this course.

# Article summary #

* Reading article from https://searchenterpriseai.techtarget.com/definition/supervised-learning
* Supervised learning is a type of system in which both input and desired output data are provided. 
* In put is labelled for classification to provide learning.
* The popular supervised machine algorithm is linear regression.
* Consider for choosing the algorithms is bias and variance. And also consider complexity of model.

# 2020-04-19 #
* Read Chapter 5 Machine Learning Basics
* Study about algorithms for learning , Overfitting and Underfitting, Hyperparameter and validation sets
* Overfitting happens when a model learns the detail and noise in the training data and Underfitting happens when model not learned enough.
* Hyperparameters are the properties for the training process to determine the struture and variables e.g. learning rate, number of hidden structure, number of epochs, hidden units, activation function. Hyperparameters are set before training stage.
* Also watch the video "What is a Neural Network chapter 1"
* The simulation of training in video helps me to illustrate how Neural Network working 
* This week I can understand the fundamental principle of machine learning after study by myself

# 2020-04-26 #
* This week I watched the 3blue1brown visualisations on youtube 
* The videos are gradient descent, backpropagation and calculus of backpropagation
* Gradient descent is optimization algorithm use to minimize the cost function. This is iterative process until reach the global minimum(the bottom of the graph). 
* Cost function or loss function is error minimize function 
* The gradient descent algorithm is in the backpropagation process and start computation from the final layer
* I studied how neural networks learn from handwritten digits recognition (MNIST Database)
* I also read chapter 5.9 Stochastic Gradient Descent from Goodfellow

# 2020-05-03 #
* I've learned about the convolutional Neural Networks
* Search more about Pooling, Rectified Linear Units(ReLUs)and Gradient descent
* Read chapter 9.1-9.3 of Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning. That is the convolution operation,motivation, and pooling.
* This week also studied some python libraries that need for deep learning such as Numpy, Pandas, Matplotlib
* Summary for how convolution neural network works: In case that input is image. CNN capture the spatial and temporal dependencies in an image through filter without losing features that will be effected to prediction performance. The kernel or filter will calulate and tranfer those pixel values to new depth of image, move from right to left and top to down until go through entire an image. For pooling layer, it is responsible for reducing the spacial size of image . The purpose is for extracting outstanding features in image. There are 2 types including max-pooling and average-pooling. Then classify the feature using softmax activation function.

# 2020-05-10 #
* Study about model validation 
* Dataset can split into 2 data including train and test data. Validation data used for evaluate model in training data while tuning hyperparameters to get the best model. This selected model willapply on test dataset to do prediction.
* Cross validation will validated on different sets of validation data that spilt from training data to aviod over-fitting problems.
* Reference: Machinelearningmastery and Medium website

# 2020-05-17 #
* Attened class and watched video about "Ethics and the danger of algorithm bias" 
* Have discussion on "Can you explain what is AI and give some example?, What do you want AI do and do not?, Do you have experience in fake news AI from real person?"
* What is  AI and give some example?: AI can do in many fields for example my interested research is doing deep learning on satellite images on-board system.
* What do you want AI do and do not?: Exchange idea with our group maid, the topic is "Do you want AI to choose your girlfriend or boyfriend?". For my opinion, I think I will let AI give me an suggestion list but i will make a dicision by myself for choosing my girlfriend or boyfriend.
* Do you have experience in fake news AI from real person?: I don't have experience about this situation.

# 2020-05-24 #
* This week, i am focusing on doing "Application of Deep Learning project".
* I use tensorflow as backend to train my model.
* I try to access MNIST dataset and used for regcognition digits. This dataset contains digits 0 to 9 (10).
* Training and testing stage run on Colaboratory from google.

# 2020-05-31 #
* Read "Automation, disruptive innovation,and the future of work" lecture and watch "The Digital Skills Gap and the Future of Jobs 2020" by Growth Tribe"
* In future work, many works will be impacted by Robotic, AI, Smart gadget, Machine learning, 3D printing etc
* Those things that I mention will also become part of every day lives
* The company or employer will replace employee by those things so a lot of employees will be laid off
* They suggest that we need to have fundamental knowledge in order to be master in technology in the future
* I think human need more skills more than usual because human will be compared with those Intelligent technology and if human don't have work's skill enough, human will take place by AI. 

# 2020-06-07 #
* This week, I am continue on doing final project.
* I improved from the previous work that it recognized just digits.
* I try to access EMNIST dataset and used for regcognition digits and also english letter. This dataset contains 62 characters including 0 to 9 (10), A to Z (26) and a to z (26).
* Training and testing stage run on Colaboratory from google.
* Using the avaliable model name DCCNN in github opensource(https://github.com/titu1994/Deep-Columnar-Convolutional-Neural-Network)
* DCCNN model is Deep-Columnar-Convolutional-Neural-Network. The total parameters are around 5 millions. The special design model is merge or concatenate between convolution layers. These convolution layers has the same channels start from 64,128,256. 
* The goal of my final project is improving the accuracy when compare to the easy structure that has 1 convolution and dense. I use the stochastic gradient descent optimizer with a learning rate of 0.001, loss is categorical_crossentropy, metrics is accuracy.   
* The dataset is EMNIST originally from MNIST dataset. This dataset has 6 sub-data including ByClass, ByMerge, Balanced, Letters, Digits, MNIST. I selected ByMerge because it has handwritten digits and also letters. Total class is 47. Uppercase Letters only : C, I, J, K, L, M, O, P, S, U,V, W, X, Y and Z and the others has uppercase and lowercase.
* This DCCNN model increase training accuracy from 88.7% to 91.11%.
* I also try 10 classes. I got 97% accuracy with DCCNN model. Therefor it implies that more class to classify it make model more challedge. That is the reason why I can get accuracy lower than 93% of 47 classes.

# 2020-06-14 #
* Article summary link: https://youtu.be/DanKh6UkRbU

# Article summary #
* Low-power neural networks for semantic segmentation of satellite images by Gatan Bahl, Lionel Daniel, Matthieu Moretti, Florent Lafarge
* This paper is semantic segmentation of images
* The goal of this paper is "Design neural network architectures operating on low-power edge devices"
* The propose architecture is c-unet and c-fcn which based on unet and fcn
* Some of convolution layers replaced by depth-wise separable convolutions to reduce the number of parameters in a convolution

