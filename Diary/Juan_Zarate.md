This diary file is written by Juan Zarate F04047244 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-02-27 #

* I learned some definitions I didn't know.
* I agree with the different definitions heard for AI but, in my opinion,  a different approach could be to consider AI as a method which purpose is to offer a solution to a problem in a highly efficient way by learning from examples.
* I like that we had an interactive class.


# 2019-03-06 #

* It�s interesting how python is easier to use than other programming languages such as C 
* I also think not having  brackets for while and for loops could get confusing 
* I think using Python programming language sounds challenging, because due to its simplicity, bugs could be harder to solve.
* I found the array and class commands a little more complicated to understand. 

# 2019-03-27 #

* It�s interesting to see how big data is one of the biggest sources of information that supply most AI applications.
* But it also comes to my mind, if there is an way to prevent the use of big data to actually change the source-of-information�s point of view.
* Principle of Parsimony sounds interesting.
