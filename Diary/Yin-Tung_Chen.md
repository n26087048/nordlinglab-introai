This diary file is written by Yin_Tung_Chen F74071190 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-20 #

* I start writing this dairy.
* I wish to know more about AI.
* I watched some successful AI story uploaded by classmates.
* I think AI is really useful and powerful.

# 2020-03-21 #

* I watch the successful AI story -- DAIN (Depth-Aware Video Frame Interpolation).
* I think AI is the most powerful technology in compurter science.
* I watched the article "What is AI?".

# 2020-04-01 #

* I watch some python materials provided by professer.
* I have some programming experience but not in python.
* I think python is really a powerful and interesting programming language.
* Hope that I will learn more about python in the futrue.

# 2020-04-08 #
* I watch some tutorial about tensorflow, which is a machine learning framework
* I think it's amazing that I can start machine learning by simple python code
* Hope I can learn more about something like this