This is a diary written by Chen Yisong F44067048 for the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists

# 2020-03-21 #

* New to BitBucket, first few days was abit rough as I was having issues creating my diary properly
* Wrote success story about Google's AlphaGo vs human player
* My inspiration of AI comes from AlphaGo and from Tony Stark's AI friend Jarvis

# 2020-04-11 #

* Professor updated everyone hes working on the videos and online classes but theres still a slight delay

# 2020-04-15 #

* First day of online class on moodle
* Professor introduced himself and each student also introduced him/herself
* Discussed improvements for future online course
* Fixed participation list for this class

# 2020-04-22 #

* No homework for this week
* Almost the same as last week, mostly Q and A.

# 2020-04-29 #

* First actual lecture
* Slides were abit complicated, also seemed as if many students were abit lost.

# 2020-05-06 #

* Second week of lectures lasted roughly 1 hour
* Content material yet again is abit complicated to understand
* Assignment of group members for each individual.

# 2020-05-13 #

* Last week of online class
* Looked at basic stuff like statistics

# 2020-05-20 #

* First day of physical class
* Meeting my group members for the first time and group discussion

# 2020-05-27 #

* No classes this date
* Met with group members during the class period to discuss the final project

# 2020-06-03 #

* Class about automation
* Each student was to think about a dream job, mines being Air Traffic Control
* Professor explained risks of jobs being replaced by AI/automation with visual representations
* Discussed with my group members about the project

# 2020-06-10 #

* No classes this date

# 2020-06-17 #

* Master and PhD students gave their presentation and we had to grade them
* Final week and preparations for next weeks presentation and final exam
