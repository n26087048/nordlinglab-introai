This diary file is written by YuShien Shen E24076255 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-22 #
 * Originally,I tought AI as a magician because  after simply feeding data to AI,I could have a model to predict or classify outcome.How amazing!
 * This week I carefully read a lecture holding an example to illustrate how really AI does.
 * Supervised AI algorithm like KNN,SVM,random forest is not  magic  but practically all math.
 * This lecture takes a house price for an example.Here are some factors like numbers of bedroom,neighborhood,area,and prices.
 * We can base on prices to determine what are the weights for every factor,but it seems impoosible if the factors are in large scales.
 * But supervised learing,they use gradient method to approach the best equation to match our data.What a intelligent method.

# 2020-03-29 # 
 * I've wathched the procedures of  Machine Learning ,which includes 10 steps.
 * The first two steps which are data collecting and data cleaning are  big lessons so I skipped this two steps.
 * I started from feature engineering including feature selection and feature extraction.
 * Why do we do feature engineering ?　It can avoid overfitting.That can simplify our model and still maintain model accurancy.
 * About feature selection,I learned about SBS(Sequential Backward Selection),Random forest,and  RFE(Recursive Feature Elimination).
 * SBS and RFE are much similar.The first one list all conbinations,eliminationg the least important factor until the number of factors reach your requirement,whereas the last one  retain the most important factor.
 * I'll update rnadom forest method because I have some questions about how it works.
 
# 2020-04-05 #
 * This week,I can use Random Forest algorithm to select factors that are  meaningful for the model,but I am still uncertain about how it works.
 * As for feature extraction,I learned PCA(Principal Component Analysis),LDA(Linear Discrimination Analysis),Kernel PCA
 * PCA is powerful in dimension reduction because in every dimenson,it find the greatest variant.Therefore,I can just use less dimension to hold my data.Interestingly,the data won't be the same as original one.
 * LDA wants the greatest similarities in groups and  the greatest discrimination out of groups using eigenvectors.
 * Kernel PCA levels up from PCA because it can divide data into groups not only linearity but also Polynomial,circular,and so on.But I can't explicitly know how it works.
 * I also try these algorithms from import "SciKitLeran" ,starting my hand-on practices. 