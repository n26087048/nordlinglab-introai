This diary is written by Eugene Lin林友鈞 student number: E64061151.

# 2019-03-21 #

* This is my first course about AI although I am a CS student and have heard AI many times.
* The article is very interesting.
* I think the current AI means Assistive Intelligence rather then Artificial Intelligence because the most AIs probabily failed the Turing test.
* The AI we currently use can only make our lives easier but it can't act like people do because there are some attributes we have but the AI does not, such as emotion.
* Therefore, I think AI can't replace people.

# 2019-3-28 #

* I'v learned python about 2 years but in the past, I usually used python to develop the backend of web. 
* In the weekend, I learned some practical packages like pandas, numpy, matplotlib and sklenrn which was recommanded while solving some regression problems