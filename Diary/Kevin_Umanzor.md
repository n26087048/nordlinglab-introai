This is my Personal diary for this Claas of Introduction for AI.
This diary file is written by Kevin Umanzor H44087074 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-05 #
We are about to start this class which is related to Artificial Intelligence, and throughout this course, we are going to learn may stuff related to programming and other stuff. Sounds really fun.

# 2020-03-12 #
The professor looks nice and I hope that maybe we can have good communication with him, even though this class is fully online.
We did fill some personal information we needed for this course, like our names, e-mails and students ID. 

# 2020-03-18 #
This is a class I wish I could have it in person becuase it really looks very interesting.
It was complicated for me to understand the program we are using right now but, I feel like I will get use to it fast.
I personally feel that AI is something that can help us to perform lots of things that can help us, not only to do some calculations and stuff, but also to make our daily-life easier.
AI is not a "Thing", but rather a Discipline.
You shouldn't compae AI's tasks because each AI is trained or programmed to do specific tasks along their work field. 

# 2020-03-25 #
I've been reading about Python lately.
Python is an interpreted language as compared to a compiled language, or as you would say, a "coding language", such as PHP, Ruby, JavaScript etc.
Interpreted languages, such as Python, allow you and helps you to develop software faster, speed of development is faster. 
Usually when people say that x language is faster than y they are usually speaking about speed of execution, not speed of development.
Speed of development is almost as important if not more than speed of execution.

# 2020-04-01 #
I read small things about the Neural Network Models, and I discovered that they are computational models that are slightly inspired by the behavior observed in their biological counterpart.
According to the information give, it is a structure that helps your recognize something.
It works with a set of units, called Neurons (or artificial neurons). Neurons form layers and they are connected to each other to transmit signals, such as the ones in our brain. They input information and they pass it through the neural network, producing output values when the information is received and processed.

# 2020-04-08 #
Accorfing to the information given about TensorFlow, we can estate that it is an open software library that is used for numerical computing with the usage of data flow graphs.
There are certain programming elements in TensorFlow that are used for writing any TensorFlow code like constants, variables, placeholder, and session.
Each element has its own syntax and functionalities. Where Constants, Variables and Placeholders are used to assign and store values, a Session is used to run a computational graph.
It was built with open source and ease of execution. It can be used and run locally and in the cloud, it is all according to which one is more convenient for you.

# 2020-04-15 #
During this day, we had our first online class. I liked the professor's accent, it sounds like a very Northern-European accent.
The video about the introduction to recognition of handqritting digits using the MNIST example in TensorFlow and Deep Learning. It was a very complete video, full of educational information about these programs.
Even though I still feel like I'm quite learning some things with this course, I don't feel like motivated on learning more coding, or at least in the near future.
Sometimes I feel like coding is vr=ery important and everybody should learn how to code, as Mark Zuckerberg once said, but I'm still not fully interested on it, but I hope that maybe someday I will find the motivation I really need for coding. For now, let's just hope that I can continue learning on this class and giving my best effort until the last day.

# 2020-04-22 #
While we are having the class online, I feel like it is good to have like a Q&A section so we can ask the professor any question that we have, but not during the whole class. I feel like that makes it a little bit dull and is not dynamic within the learning of the class.
What I learned about data processing during this week is that, when data is collected, ready and translated into useful information, then Data Processing is taking place.
Data processing starts with data in its raw form and converts it into a more readable format (graphs, documents, etc.), giving it the form and context necessary to be interpreted by computers and utilized by employees throughout an organization.
The future of data processing lies in the cloud. Cloud technology builds on the convenience of current electronic data processing methods and accelerates its speed and effectiveness.
The six stages of data processing are: Data collection, data preparation, data entry, data processing, data interpretation and data storage.

# 2020-04-29 #
The course is hard, really hard to understand. I'm trying to get everything I can but it is very hard to keep teaching myself.
Even though at the beginning I though it was actually going to be an easy class, I've discovered it's not, and now, I'm struggling to understand the content. And the worse part, is that I don't have any previous knowledge in all this stuff.
This week, I couldn't read that much.

# 2020-05-06 #
I'm kind of excited because the group project sounds really interesting, but I don't like the fact that we are beeing grouped with people we don't even know... in an online class. What if they don't even answer my e-mails?
Today's topic was about programming, and eventhough it was scary what I saw during the class, I think that maybe I can be able to catch and understand that information in the remaining two months of the semester.

# 2020-05-13 #
So, we have been grouped already and I don't see any other foreigner in my group. I feel like I will be excluded jsut for the fact that they are all Taiwanese and they might not be happy to know that they need to discuss every single thing they do in English. I hope that I can ask the professor to change me to another group during this next two weeks.
Still kind of nervous on how we are going to do the group project AND the final exam.

# 2020-05-20 #
The professor finally agreed on having physical class and I feel like we had a lot of interaction with our classmates. We were asked several questions and we needed to answer them out loud after discussing it with our classmates. I've never felt so happy in this class before.
I'm also happy for the fact that the professor changed me to another group without me even asking him. Apparently no other team member from my group came to my class, so now I'm in the group #2, and we are all foreigners, but, none of us have previous knwoledge about coding.
Even though we don't know how to code, I still hope we can overcome our project succesfully.
While discussing things during the class, we were talking about the importance of AI in our daily life and how sometimes they can be dangerous and need to be stopped. We need to be careful everytime we program them and everytime we decide that they should help us in our daily life.

# 2020-05-27 #
The automation revolution is raising increasingly interesting questions about the nature of the workplace – and about what it might look like in the future.
Thinking about Automation and the future of our works actually gives me some kind of chills because I think that someday, AI will become so advance and so useful that it will be better for companies to have AI robots and systems on doing work, and not humans, leading to a very big unemployment rate, not only in a country, but all around the world.
Opportunities for artificial intelligence are increasingly exponentiallu with opportunities for human intelligence in order to establish a new total efficiency of effort.
"It will in particular involve human beings taking on the challenge of material that automation cannot handle: the exceptional, the anomalous and the problematic.", says a user on the internet.